Hooks.on('init', () => {

    if (typeof Babele !== 'undefined') {
        Babele.get().register({
            module: 'Shadowrun5-Fr',
            lang: 'fr',
            dir: 'compendium'
        });
    }
});